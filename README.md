README for member of this academic project.

If u can read this without autorization you are a fuckin hacker. Greetings.

####### NOW THERE IS SOME FEATURES THAT HAS TO BE ADDED TO OUR APPLICATION FELLOWS #####

AGGIUNTA DI FOLLOWER
Widget con elenco completo degli utenti registrati tutti cliccabili, una volta cliccato appare l'opzione di follow.



####QUI DI SEGUITO CI SARANNO LE SPECIFICHE DEL PROGETTO A PAROLE#######
VSSN: Very Simple Social Network
Versione 1.0
Implementare un’applicazione Web che supporti le funzionalità di base di un social network.
Nello specifico, l’applicazione dovrà permettere ad ogni utente di:
registrarsi; 
inserire messaggi; 
diventare “follower” di altri utenti; 
leggere i messaggi inseriti dagli altri utenti. 

**Registrazione di un utente**
Durante la registrazione al sito l’utente dovrà specificare:
il proprio nome o nickname 
il luogo di nascita o di residenza 
la data di nascita 
il sesso 
il proprio username 
la password 
la propria email (opzionale) 
Una volta registrato l’utente non potrà più modificare i propri dati. Non è previsto un meccanismo di recupero della password.

**Inserimento di messaggi **
Questa funzionalità è riservata agli utenti registrati. Per ogni messaggio inserito l’utente dovrà specificare la categoria alla quale appartiene il messaggio.
Un messaggio potrà essere esclusivamente di testo e potrà avere una lunghezza massima di 500 caratteri. Un messaggio una volta inserito potrà essere rimosso solo dall’amministratore.

**Followership**
L’utente registrato potrà scegliere di diventare follower di altri utenti: tali utenti entreranno quindi a far parte della propria following list.
In ogni momento l’utente potrà decidere di rimuovere o aggiungere uno o più utenti dalla/alla propria following list.
Gestione delle categorie
L’aggiunta o la rimozione delle categorie potrà essere effettuata dall’utente amministratore. La rimozione di una categoria sarà possibile solo se la categoria risulta essere priva di messaggi. Se nel sistema non è presente neanche una categoria non potranno essere inseriti nuovi messaggi.

**
Lettura di messaggi **
La lettura dei messaggi sarà possibile sia agli utenti registrati che (utenti)non registrati.
Gli utenti non registrati avranno la possibilità di scegliere, ogni volta che accedono alla pagina di visualizzazione dei messaggi, se visualizzare tutti i messaggi o visualizzare solo i messaggi di una certa categoria. Gli utenti registrati, in aggiunta, avranno anche la possibilità di visualizzare solo i messaggi inseriti da un proprio following. Per gli utenti registrati quindi dovrà anche essere possibile visualizzare solo i messaggi scritti da un proprio following e appartenenti ad una certa categoria.
La pagina di visualizzazione dei messaggi dovrà quindi contenere, oltre ad un’area principale  dove saranno visualizzati i messaggi in ordine cronologico, anche due menù a tendina dai quali sarà possibile selezionare le categorie (o tutte o una specifica) e, per gli utenti registrati, i following (o tutti o uno specifico).
Si fa presente che alcuni dati (ad esempio i messaggi e le informazioni relative agli utenti) dovranno essere accessibili anche a seguito del riavvio del server. 



