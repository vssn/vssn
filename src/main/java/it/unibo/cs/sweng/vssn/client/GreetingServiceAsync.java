package it.unibo.cs.sweng.vssn.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import it.unibo.cs.sweng.vssn.domain.Messaggio;
import it.unibo.cs.sweng.vssn.domain.Utente;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	void greetServer(String input, AsyncCallback<String> callback)
			throws IllegalArgumentException;

	void passaUtente(Utente utente, AsyncCallback<Void> callback);

	void salvaMessaggio(Messaggio messaggio, AsyncCallback<Void> callback);

	void getAllMessaggio(AsyncCallback<ArrayList<Messaggio>> callback);

	void login(String name, String password, AsyncCallback<Utente> callback);

	void loginFromSessionServer(AsyncCallback<Utente> callback);

	void getAllUtente(AsyncCallback<ArrayList<Utente>> callback);

	void getAllCategoria(AsyncCallback<ArrayList<String>> callback);

	void salvaCategoria(String categoria, AsyncCallback<Void> callback);

	void deleteCategoria(String categoria, AsyncCallback<Void> callback);

	void logoutFromSessionServer(AsyncCallback<Void> callback);

	void addFollowing(String following, AsyncCallback<Void> callback);

	void getAllFollowing(AsyncCallback<ArrayList<String>> callback);

	void removeFollowing(String following, AsyncCallback<Void> callback);

	void deleteMessaggio(int idMessaggio, AsyncCallback<Void> callback);



	

	

	

	
}
