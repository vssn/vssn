package it.unibo.cs.sweng.vssn.domain;

import java.io.Serializable;

public class Messaggio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 856045819417099075L;

	private int idMessaggio;
	private String idUtente;
	private String categoria;
	private String msg;
	
	
	public Messaggio(){
		
	}
	
	public int getIdMessaggio() {
		return idMessaggio;
	}
	public void setIdMessaggio(int idMessaggio) {
		this.idMessaggio = idMessaggio;
	}
	public String getIdUtente() {
		return idUtente;
	}
	public void setIdUtente(String idUtente) {
		this.idUtente = idUtente;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	@Override
	public String toString() {
		return "Messaggio [idMessaggio=" + idMessaggio + ", idUtente="
				+ idUtente + ", msg=" + msg + "]";
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	
}
