package it.unibo.cs.sweng.vssn.domain.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;




import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.unibo.cs.sweng.vssn.domain.Messaggio;


@Component
public class MessaggioRepositoryImpl implements MessaggioRepository {

	Logger logger = Logger.getLogger(MessaggioRepositoryImpl.class);

	@Autowired
	private DataSource<DB> dataSource;
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Messaggio delete(Integer id) {
		logger.info("Cancellazione in corso");
		DB db = dataSource.getDb();				
		
		Map<Integer, Messaggio> map = db.getTreeMap("messaggi");
		Messaggio msg = map.remove(id);
		
		db.commit();
		
		return msg;
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean exists(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Messaggio findOne(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer save(Messaggio entity) {
		logger.info("Salvataggio in corso");
		DB db = dataSource.getDb();				
		
		logger.info("Salvataggio su disco riuscito");
		
		Map<Integer, Messaggio> map = db.getTreeMap("messaggi");
		//map.clear();
		
		
		int idMessaggio = map.size();
		while(map.containsKey(idMessaggio)){
			idMessaggio++;
		}
		
		logger.info("idMessaggio is "+ idMessaggio);
		entity.setIdMessaggio(idMessaggio);
		map.put(idMessaggio, entity);
		
		db.commit();
		//db.close();		
		
		
		return idMessaggio;
	}

	@Override
	public List<Messaggio> findAllByUsername(String username) {
		DB db = dataSource.getDb();
		Map<Integer, Messaggio> map = db.getTreeMap("messaggio");
		
		List<Messaggio> result = new ArrayList<Messaggio>();


		for (Map.Entry<Integer, Messaggio> entry : map.entrySet())
		{
			if (entry.getValue().getIdUtente().equals(username)){
				result.add(entry.getValue());
			}
		}


		
		return result;
	}

	@Override
	public Collection<Messaggio> getAll() {
		logger.info("Prelevo di tutti i messaggi");
		
		DB db = dataSource.getDb();				
		
		Map<Integer, Messaggio> map = db.getTreeMap("messaggi");
		
		Collection<Messaggio> result = map.values();
		//db.close();		
		
		
				
		
		return result;
	}



}
