package it.unibo.cs.sweng.vssn.domain.repository;

import java.util.List;

import it.unibo.cs.sweng.vssn.domain.Messaggio;

public interface MessaggioRepository extends CrudRepository<Integer, Messaggio> {
	List<Messaggio> findAllByUsername(String username);
}
