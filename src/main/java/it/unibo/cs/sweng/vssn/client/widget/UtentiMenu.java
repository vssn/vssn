package it.unibo.cs.sweng.vssn.client.widget;

import java.util.ArrayList;
import java.util.Collection;

import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.client.GreetingServiceAsync;
import it.unibo.cs.sweng.vssn.domain.Utente;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.widget.client.TextButton;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasVerticalAlignment;



public class UtentiMenu extends Composite {
	
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	private PageLayout mainPage;
	
	public UtentiMenu() {
		
		
		CaptionPanel cptnpnlUserMenu = new CaptionPanel();
		cptnpnlUserMenu.setStyleName("utentiMenu");
		cptnpnlUserMenu.setCaptionText("User Menu");
		initWidget(cptnpnlUserMenu);
		cptnpnlUserMenu.setWidth("100%");
		
		FlexTable flexTable = new FlexTable();
		cptnpnlUserMenu.setContentWidget(flexTable);
		flexTable.setSize("100%", "100%");
		
		TextButton txtbtnGestioneFollowing = new TextButton("Gestisci Following");
		flexTable.setWidget(0, 0, txtbtnGestioneFollowing);
		flexTable.getCellFormatter().setVerticalAlignment(0, 0, HasVerticalAlignment.ALIGN_TOP);
		
		
		txtbtnGestioneFollowing.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				mainPage.getCenter().clear();
				mainPage.getCenter().add(new UtentiFollowingForm(),DockPanel.CENTER);
				
			}
			
		});
		
	}

	public void setMainPage(PageLayout mainPage) {
		this.mainPage = mainPage;
	}

}
