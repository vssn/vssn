package it.unibo.cs.sweng.vssn.client.widget;

import java.util.ArrayList;

import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.client.GreetingServiceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.TextBox;

public class CategorieForm extends Composite {
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	public CategorieForm(){
		
		CaptionPanel cptnpnlNewPanel = new CaptionPanel("Gestione Categorie");
		initWidget(cptnpnlNewPanel);
		
		FlexTable flexTable = new FlexTable();
		cptnpnlNewPanel.setContentWidget(flexTable);
		flexTable.setSize("100%", "100%");
		
		final TextBox textBox = new TextBox();
		flexTable.setWidget(0, 0, textBox);
		textBox.setWidth("100%");
		
		Button btnAdd = new Button("Add");
		flexTable.setWidget(0, 1, btnAdd);
		
		btnAdd.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				greetingService.salvaCategoria(textBox.getText(), new AsyncCallback<Void>(){

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(Void result) {
						Window.Location.reload();
						
					}
					
				});
				
			}
			
		});
		
		final ListBox listBox = new ListBox();
		flexTable.setWidget(1, 0, listBox);
		listBox.setSize("100%", "100%");
		listBox.setVisibleItemCount(5);
		
		
		greetingService.getAllCategoria(new AsyncCallback<ArrayList<String>>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("ERROR ON LOAD CATEGORIE");
				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				for(int i=0;i<result.size();i++){
					listBox.addItem(result.get(i));
				}
				
			}
			
		});
		
		Button btnRemove = new Button("Remove");
		flexTable.setWidget(1, 1, btnRemove);
		
		btnRemove.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				greetingService.salvaCategoria(listBox.getItemText(listBox.getSelectedIndex()), new AsyncCallback<Void>(){

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("ERROR OCCURRED");
						
					}

					@Override
					public void onSuccess(Void result) {
						greetingService.deleteCategoria(listBox.getItemText(listBox.getSelectedIndex()), new AsyncCallback<Void>(){

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								
							}

							@Override
							public void onSuccess(Void result) {
								Window.Location.reload();
								
							}
							
						});
						
					}
					
				});
				
			}
			
		});
		flexTable.getCellFormatter().setVerticalAlignment(1, 1, HasVerticalAlignment.ALIGN_TOP);
		
	}
}
