package it.unibo.cs.sweng.vssn.domain.repository;

import java.util.ArrayList;

public interface FollowingRepository extends CrudRepository<String, ArrayList<String>> {
	void addFollowing(String follower, String following);
	
	void removeFollowing(String follower, String following);
	
	ArrayList<String> getAllByUsername(String username);
}
