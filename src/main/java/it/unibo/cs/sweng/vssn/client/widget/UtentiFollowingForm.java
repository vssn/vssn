package it.unibo.cs.sweng.vssn.client.widget;

import java.util.ArrayList;

import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.client.GreetingServiceAsync;
import it.unibo.cs.sweng.vssn.domain.Utente;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.TextBox;

public class UtentiFollowingForm extends Composite {
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	final ListBox listBox;
	
	public UtentiFollowingForm(){
		
		CaptionPanel cptnpnlNewPanel = new CaptionPanel("Gestione Categorie");
		initWidget(cptnpnlNewPanel);
		
		FlexTable flexTable = new FlexTable();
		cptnpnlNewPanel.setContentWidget(flexTable);
		flexTable.setSize("100%", "100%");
		
		final ListBox comboBox = new ListBox();
		flexTable.setWidget(0, 0, comboBox);
		comboBox.setWidth("100%");
		
		greetingService.getAllUtente(new AsyncCallback<ArrayList<Utente>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<Utente> result) {
				for(int i=0;i<result.size();i++){
					comboBox.addItem(result.get(i).getUsername());
				}
				
			}
			
		});
		
		
		Button btnAdd = new Button("Add");
		flexTable.setWidget(0, 1, btnAdd);
		
		btnAdd.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				
				
				greetingService.addFollowing(comboBox.getItemText(comboBox.getSelectedIndex()), new AsyncCallback<Void>(){

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("ERROR OCCURRED");
						
					}

					@Override
					public void onSuccess(Void result) {
						listBox.addItem(comboBox.getItemText(comboBox.getSelectedIndex()));
						
					}
					
				});
				
			}


			
		});
		
		listBox = new ListBox();
		flexTable.setWidget(1, 0, listBox);
		listBox.setSize("100%", "100%");
		listBox.setVisibleItemCount(5);
		
		greetingService.getAllFollowing(new AsyncCallback<ArrayList<String>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				for(int i=0;i<result.size();i++){
					listBox.addItem(result.get(i));
				}
				
			}
			
		});

		
		Button btnRemove = new Button("Remove");
		flexTable.setWidget(1, 1, btnRemove);
		
		btnRemove.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				greetingService.removeFollowing(listBox.getItemText(listBox.getSelectedIndex()), new AsyncCallback<Void>(){

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("ERROR OCCURRED");
						
					}

					@Override
					public void onSuccess(Void result) {
						listBox.removeItem(listBox.getSelectedIndex());
						
					}
					
				});
				
			}
			
		});
		flexTable.getCellFormatter().setVerticalAlignment(1, 1, HasVerticalAlignment.ALIGN_TOP);
		
	}
}
