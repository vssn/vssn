package it.unibo.cs.sweng.vssn.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import it.unibo.cs.sweng.vssn.domain.Messaggio;
import it.unibo.cs.sweng.vssn.domain.Utente;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gwt.ss.client.exceptions.GwtSecurityException;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	String greetServer(String name) throws IllegalArgumentException;
	
	void passaUtente(Utente utente);
	ArrayList<Utente> getAllUtente() throws GwtSecurityException;
	
	void salvaMessaggio(Messaggio messaggio);	
	ArrayList<Messaggio> getAllMessaggio() throws GwtSecurityException;
	void deleteMessaggio(int idMessaggio);
	
	Utente login(String username, String password);	
	Utente loginFromSessionServer();
	void logoutFromSessionServer();
	
	ArrayList<String> getAllCategoria() throws GwtSecurityException;
	void salvaCategoria(String categoria);
	void deleteCategoria(String categoria);
	
	void addFollowing(String following);
	void removeFollowing(String following);
	ArrayList<String> getAllFollowing();
	
	
}
