package it.unibo.cs.sweng.vssn.client.widget;

import java.util.ArrayList;

import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.client.GreetingServiceAsync;
import it.unibo.cs.sweng.vssn.domain.Messaggio;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.i18n.client.HasDirection.Direction;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.widget.client.TextButton;

public class MessageForm extends Composite {
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	private Button btnPost;
	private TextArea msgTextArea;
	private Label lblUsername;
	private ListBox comboBox;
	private HorizontalPanel horizontalPanel;
	private Label lblCategory;
	private Hidden idMessaggio;
	private TextButton txtbtnDelete;
	
	public MessageForm() {
		
		FlexTable flexTable = new FlexTable();
		flexTable.setStyleName("messageBoard");
		initWidget(flexTable);
		flexTable.setSize("100%", "100%");
		
		idMessaggio = new Hidden("Hidden name");
		flexTable.setWidget(0, 0, idMessaggio);
		
		lblUsername = new Label("Username");
		flexTable.setWidget(1, 0, lblUsername);
		
		msgTextArea = new TextArea();
		flexTable.setWidget(2, 0, msgTextArea);
		msgTextArea.setSize("357px", "87px");
		
		horizontalPanel = new HorizontalPanel();
		flexTable.setWidget(3, 0, horizontalPanel);
		
		comboBox = new ListBox();
		horizontalPanel.add(comboBox);
		
		lblCategory = new Label("category");
		lblCategory.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		horizontalPanel.add(lblCategory);
		lblCategory.setSize("263px", "24px");
		
		greetingService.getAllCategoria(new AsyncCallback<ArrayList<String>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				for(int i=0;i<result.size();i++){
					comboBox.addItem(result.get(i));
				}
				
			}
			
		});
		
		btnPost = new Button("Post");
		flexTable.setWidget(4, 0, btnPost);
		
		txtbtnDelete = new TextButton("Delete");
		flexTable.setWidget(5, 0, txtbtnDelete);
		
		txtbtnDelete.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				greetingService.deleteMessaggio(Integer.parseInt(idMessaggio.getDefaultValue()), new AsyncCallback<Void>(){

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("An error occurred");
						
					}

					@Override
					public void onSuccess(Void result) {
						Window.alert("Messaggio eliminato!");
						
					}
					
				});
				
			}
			
		});
		
		btnPost.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				Messaggio messaggio = new Messaggio();
				
				messaggio.setMsg(msgTextArea.getText());
				messaggio.setCategoria(comboBox.getItemText(comboBox.getSelectedIndex()));
				
				greetingService.salvaMessaggio(messaggio, new AsyncCallback<Void>(){

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("ERROR OCCURRED");
						
					}

					@Override
					public void onSuccess(Void result) {
						Window.alert("Messaggio salvato");
						
					}
					
				});
				
			}
			
		});
	}

	
	public TextArea getMsgTextArea() {
		return msgTextArea;
	}


	public Button getBtnPost(){
		return btnPost;
	}


	public ListBox getComboBox() {
		return comboBox;
	}


	public Label getLblCategory() {
		return lblCategory;
	}


	public Label getLblUsername() {
		return lblUsername;
	}


	public Hidden getIdMessaggio() {
		return idMessaggio;
	}


	public void setIdMessaggio(Hidden idMessaggio) {
		this.idMessaggio = idMessaggio;
	}


	public TextButton getTxtbtnDelete() {
		return txtbtnDelete;
	}


	public void setTxtbtnDelete(TextButton txtbtnDelete) {
		this.txtbtnDelete = txtbtnDelete;
	}
	
	
	
	
}
