package it.unibo.cs.sweng.vssn.client.widget;

import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.client.GreetingServiceAsync;
import it.unibo.cs.sweng.vssn.domain.Utente;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.client.ui.ListBox;

public class RegisterForm extends Composite {
	
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	public RegisterForm() {
		
		FormPanel formPanel = new FormPanel();
		formPanel.setMethod(FormPanel.METHOD_POST);
		
		formPanel.setSize("100%", "100%");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		formPanel.setWidget(verticalPanel);
		verticalPanel.setSize("100%", "100%");
		
		FlexTable flexTable = new FlexTable();
		verticalPanel.add(flexTable);
		flexTable.setWidth("100%");
		
		Label lblNome = new Label("Nome");
		flexTable.setWidget(0, 0, lblNome);
		
		final TextBox txtNome = new TextBox();
		flexTable.setWidget(0, 1, txtNome);
		
		Label lblCognome = new Label("Username");
		flexTable.setWidget(1, 0, lblCognome);
		
		final TextBox textUser = new TextBox();
		flexTable.setWidget(1, 1, textUser);
		
		Button btnRegistrati = new Button("Registrati");
		
		Label lblEmail = new Label("Email");
		flexTable.setWidget(2, 0, lblEmail);
		
		final TextBox textEmail = new TextBox();
		flexTable.setWidget(2, 1, textEmail);
		
		Label lblPassword = new Label("Password");
		flexTable.setWidget(3, 0, lblPassword);
		
		final PasswordTextBox passwordTextBox = new PasswordTextBox();
		flexTable.setWidget(3, 1, passwordTextBox);
		
		Label lblResidenza = new Label("Residenza");
		flexTable.setWidget(4, 0, lblResidenza);
		
		final TextBox textResidenza = new TextBox();
		flexTable.setWidget(4, 1, textResidenza);
		
		Label lblDataNascita = new Label("Data nascita");
		flexTable.setWidget(5, 0, lblDataNascita);
		
		final DateBox dateBox = new DateBox();
		flexTable.setWidget(5, 1, dateBox);
		
		Label lblSesso = new Label("Sesso");
		flexTable.setWidget(6, 0, lblSesso);
		
		final ListBox comboBox = new ListBox();
		flexTable.setWidget(6, 1, comboBox);
		
		comboBox.addItem("M");
		comboBox.addItem("F");
		
		flexTable.setWidget(7, 0, btnRegistrati);
		
		DecoratorPanel decoratorPanel = new DecoratorPanel();
		decoratorPanel.add(formPanel);
		
		btnRegistrati.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Utente utente = new Utente();
				utente.setNome(txtNome.getText());
				utente.setUsername(textUser.getText());
				utente.setEmail(textEmail.getText());
				utente.setPassword(passwordTextBox.getValue());
				utente.setResidenza(textResidenza.getText());
				utente.setDataNascita(dateBox.getValue());
				utente.setSesso(comboBox.getItemText(comboBox.getSelectedIndex()));
				greetingService.passaUtente(utente, new AsyncCallback<Void>(){

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("ERROR OCCURRED");
						
					}

					@Override
					public void onSuccess(Void result) {
						Window.alert("Utente registrato correttamente");
						Window.Location.reload();
						
					}


					
				});
				
				
			}
		});

		
		initWidget(decoratorPanel);
	}

}
