package it.unibo.cs.sweng.vssn.domain;

import java.io.Serializable;
import java.util.Date;

//import sun.security.util.Password;

public class Utente implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5077198425829870334L;
	private String nome;
	private String residenza;
	private Date dataNascita;
	private String sesso;
	private String username;
	private String password;
	private String email;
	
	public Utente() {
		
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getResidenza() {
		return residenza;
	}
	public void setResidenza(String residenza) {
		this.residenza = residenza;
	}
	public Date getDataNascita() {
		return dataNascita;
	}
	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}
	public String getSesso() {
		return sesso;
	}
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Utente [nome=" + nome + ", residenza=" + residenza
				+ ", dataNascita=" + dataNascita + ", sesso=" + sesso
				+ ", username=" + username + ", password=" + password
				+ ", email=" + email + "]";
	}
	
	
	
	
}
