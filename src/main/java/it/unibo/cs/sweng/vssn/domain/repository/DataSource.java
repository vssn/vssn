package it.unibo.cs.sweng.vssn.domain.repository;

public interface DataSource<T> {

	
	T getDb();
	
}
