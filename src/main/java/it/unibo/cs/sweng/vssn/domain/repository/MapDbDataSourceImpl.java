package it.unibo.cs.sweng.vssn.domain.repository;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;

@Component
public class MapDbDataSourceImpl implements DataSource<DB>{
	
	Logger logger = Logger.getLogger(MapDbDataSourceImpl.class);
	
	private ServletContext servletContext;
	
	public void setServletContext(ServletContext sc){
		this.servletContext=sc;
	}
	
	private DB db;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@Value("${mapdb.source}")
	private String mapdbFile;
	
	
	@PostConstruct
	public void initIt() throws Exception {
		logger.info("Apertura file mapdb");
		
		servletContext = webApplicationContext.getServletContext();
		
		
		
		URL url=null;
		try {
			url = servletContext.getResource("/");

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		logger.info("mapdbFile is: " + mapdbFile);
		

		logger.info("url: " + url.getPath());
		
		logger.info("MapDbFile is " + url.getPath()+mapdbFile);
		db= DBMaker.newFileDB(new File(url.getPath()+mapdbFile)).closeOnJvmShutdown().randomAccessFileEnable().make();
	}
	
	@PreDestroy
	public void cleanUp() throws Exception {
	  db.close();
	}
	
	@Override
	public DB getDb() {
		return this.db;

				
	}

}
