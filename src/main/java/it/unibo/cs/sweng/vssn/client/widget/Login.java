package it.unibo.cs.sweng.vssn.client.widget;

import org.springframework.beans.factory.annotation.Value;

import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.client.GreetingServiceAsync;
import it.unibo.cs.sweng.vssn.domain.Utente;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.PasswordTextBox;

public class Login extends Composite {
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	@Value("${baseUrl}")
	private String baseUrl;
	 
	public Login() {
		
		FlexTable flexTable = new FlexTable();
		initWidget(flexTable);
		flexTable.setHeight("135px");
		
		Label lblNewLabel = new Label("Username");
		flexTable.setWidget(0, 0, lblNewLabel);
		
		final TextBox textBox = new TextBox();
		flexTable.setWidget(0, 1, textBox);
		
		Label lblPassword = new Label("Password");
		flexTable.setWidget(1, 0, lblPassword);
		
		final PasswordTextBox textBox_1 = new PasswordTextBox();
		flexTable.setWidget(1, 1, textBox_1);
		
		Button btnEntern = new Button("Login");
		flexTable.setWidget(2, 1, btnEntern);
		
		
		btnEntern.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				greetingService.login(textBox.getText(), textBox.getText(), new AsyncCallback<Utente>(){

					@Override
					public void onFailure(Throwable caught) {
					
							
						Window.alert("ERROR OCCURRED");
						
					}

					@Override
					public void onSuccess(Utente result) {
						if (result!=null){
							//Window.alert("Utente not null");
						} else {
							Window.alert("Utente autenticato correttamente!");
						}
						//Window.alert("UTENTE LOGGATO IS: " + result.toString());
						Window.Location.assign("http://127.0.0.1:8888/");
						
					}
					
				});
				
			}
			
		});
	}

}
