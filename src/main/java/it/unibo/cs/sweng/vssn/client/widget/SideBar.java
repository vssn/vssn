package it.unibo.cs.sweng.vssn.client.widget;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.Hyperlink;

public class SideBar extends Composite {
	
	private DecoratedStackPanel decoratedStackPanel;
	
	public SideBar() {
		
		decoratedStackPanel = new DecoratedStackPanel();
		initWidget(decoratedStackPanel);
		decoratedStackPanel.setWidth("100%");
		
		
	}
	
	public DecoratedStackPanel getDecoratedStackPanel(){
		return decoratedStackPanel;
	}

}
