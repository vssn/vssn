package it.unibo.cs.sweng.vssn.client.widget;

import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.client.GreetingServiceAsync;
import it.unibo.cs.sweng.vssn.domain.Messaggio;
import it.unibo.cs.sweng.vssn.domain.Utente;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;

public class MessageBoard extends Composite {
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	FlexTable board;
	
	
	public MessageBoard() {
		
		board = new FlexTable();
		initWidget(board);
		
		greetingService.getAllMessaggio(new AsyncCallback<ArrayList<Messaggio>>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("error");
				
			}

			@Override
			public void onSuccess(ArrayList<Messaggio> result) {
				
				for(int i=0;i<result.size();i++){
					final MessageForm msg = new MessageForm();
					msg.getMsgTextArea().setText(result.get(i).getMsg());
					msg.getMsgTextArea().setEnabled(false);
					msg.getLblCategory().setText(result.get(i).getCategoria());
					msg.getComboBox().setVisible(false);
					msg.getBtnPost().setVisible(false);
					msg.getTxtbtnDelete().setVisible(false);
					
					greetingService.loginFromSessionServer(new AsyncCallback<Utente>(){

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(Utente result) {
							if(result.getUsername().equals("admin")){
								msg.getTxtbtnDelete().setVisible(true);
							} else {
								msg.getTxtbtnDelete().setVisible(false);
							}
								
							
						}


						
					});
					
					
					msg.getLblUsername().setText(result.get(i).getIdUtente());
					msg.getIdMessaggio().setDefaultValue(Integer.toString(result.get(i).getIdMessaggio()));
					Window.alert("hidden idMessaggio is " + Integer.toString(result.get(i).getIdMessaggio()));
					board.setWidget(i, 0, msg);
					
				}
				

			}


			
		});
	}
	
	public FlexTable getBoard(){
		return board;
	}

	
	public void setBoard(final String category, final String username){
		board.clear();
		
		greetingService.getAllMessaggio(new AsyncCallback<ArrayList<Messaggio>>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("error");
				
			}

			@Override
			public void onSuccess(ArrayList<Messaggio> result) {
				
				for(int i=0;i<result.size();i++){
					if( (result.get(i).getCategoria().equals(category) || category.equals("ALL")) && (result.get(i).getIdUtente().equals(username) || username.equals("ALL"))){
						MessageForm msg = new MessageForm();					
						msg.getMsgTextArea().setText(result.get(i).getMsg());
						msg.getMsgTextArea().setEnabled(false);
						msg.getLblCategory().setText(result.get(i).getCategoria());
						msg.getComboBox().setVisible(false);
						msg.getBtnPost().setVisible(false);
						msg.getBtnPost().setVisible(false);
						msg.getLblUsername().setText(result.get(i).getIdUtente());
						board.setWidget(i, 0, msg);
					}

					
				}
				

			}


			
		});	
	}

	
}
