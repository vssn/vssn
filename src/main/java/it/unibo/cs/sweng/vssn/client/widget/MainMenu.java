package it.unibo.cs.sweng.vssn.client.widget;

import org.springframework.beans.factory.annotation.Value;

import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.client.GreetingServiceAsync;
import it.unibo.cs.sweng.vssn.domain.Utente;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.widget.client.TextButton;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;

	

public class MainMenu extends Composite {
	
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	private PageLayout mainPage;
	
	private TextButton txtbtnHome;
	private TextButton txtbtnRegister;
	private TextButton txtbtnLogin;
	private TextButton txtbtnLogout;
	
	@Value("${baseUrl}")
	private String baseUrl;
	
	public MainMenu() {
		
		CaptionPanel verticalPanel = new CaptionPanel();
		verticalPanel.setStyleName("mainMenu");
		verticalPanel.setCaptionHTML("Main Menu");
		verticalPanel.setCaptionText("Main Menu");
		initWidget(verticalPanel);
		verticalPanel.setWidth("100%");
		
		FlexTable flexTable = new FlexTable();
		verticalPanel.add(flexTable);
		
		txtbtnHome = new TextButton("Home");
		
		txtbtnHome.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				
				mainPage.getCenter().clear();				
				
				
				greetingService.loginFromSessionServer(new AsyncCallback<Utente>(){

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(Utente result) {
						if(result!=null){
							MessageForm messageForm = new MessageForm();
							messageForm.getLblCategory().setVisible(false);
							messageForm.getLblUsername().setVisible(false);
							messageForm.getTxtbtnDelete().setVisible(false);
							mainPage.getCenter().add(messageForm,DockPanel.CENTER);
						}
						
					}
					
				});
				

				
				

				mainPage.getCenter().add(mainPage.getMessageBoard(),DockPanel.SOUTH);
				
			}
							
		});

		flexTable.setWidget(0, 0, txtbtnHome);
		
		txtbtnRegister = new TextButton("Register");
		flexTable.setWidget(1, 0, txtbtnRegister);
		
		
		
		txtbtnRegister.addClickHandler(new ClickHandler(){
		
			@Override
			public void onClick(ClickEvent event) {
				mainPage.getCenter().clear();
				mainPage.getCenter().add(new RegisterForm(),DockPanel.CENTER);
				
			}
			
		});
		

		
		

		
		
		txtbtnLogin = new TextButton("Login");
		flexTable.setWidget(2, 0, txtbtnLogin);
		
		txtbtnLogin.addClickHandler(new ClickHandler() {
		
			@Override
			public void onClick(ClickEvent event) {
				mainPage.getCenter().clear();
				mainPage.getCenter().add(new Login(),DockPanel.CENTER);
				
			}
			
			
		});
		
		
		txtbtnLogout = new TextButton("Logout");
		flexTable.setWidget(3, 0, txtbtnLogout);
		
		txtbtnLogout.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				greetingService.logoutFromSessionServer(new AsyncCallback<Void>(){

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(Void result) {
						
						Window.Location.reload();
								//"http://127.0.0.1:8888/");
						
					}
					
				});
				
			}
			
		});
		
	}
	
	public TextButton getTxtbtnHome() {
		return txtbtnHome;
	}	
	
	public TextButton getTxtbtnRegister(){
		return txtbtnRegister;
	}
	
	public TextButton getTxtbtnLogin(){
		return txtbtnLogin;
	}

	public void setMainPage(PageLayout mainPage) {
		this.mainPage = mainPage;
	}





}
