package it.unibo.cs.sweng.vssn.domain.repository;

import java.util.Collection;
import java.util.List;

public interface CrudRepository<ID, T> {
	/**
	 * Returns the number of entities available. 
	 */
	long count();
	
	/**
	 * Deletes an entity by id.
	 */
	T delete(ID id);
	
	/**
	 * Deletes all entities.
	 */
	void deleteAll();
	
	/**
	 * Returns whether an entity with the given id exists.
	 */
	boolean exists(ID id);
	
	/**
	 * Retrieves an entity by its id.
	 */
	T findOne(ID id);
	
	Collection<T> getAll();
	
	/**
	 * Saves a given entity.
	 */
	ID save(T entity);

	
	
}
