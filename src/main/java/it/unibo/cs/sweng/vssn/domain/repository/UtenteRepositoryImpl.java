package it.unibo.cs.sweng.vssn.domain.repository;

import it.unibo.cs.sweng.vssn.domain.Messaggio;
import it.unibo.cs.sweng.vssn.domain.Utente;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;



//@Repository
@Component
public class UtenteRepositoryImpl implements UtenteRepository {
	Logger logger = Logger.getLogger(UtenteRepositoryImpl.class);
	
	@Autowired
	private DataSource<DB> dataSource;
	
	private String mapUtenti = "utenti2";
	
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Utente delete(String id) {
		logger.info("Deleting Utente with username: " + id);

	    DB db = dataSource.getDb();
	    Map<String, Utente> map = db.getTreeMap(mapUtenti);
		
		Utente utente = map.remove(id);		
		db.commit();		
		//db.close();
		
		return utente;
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
		
		
	}

	@Override
	public boolean exists(String id) {
		logger.info("Checking if " + id + "exists.");
		
	    DB db = dataSource.getDb();
	    Map<String, Utente> map = db.getTreeMap(mapUtenti);
	
	    Set<String> keys = map.keySet();
	    
	    //db.close();
	    
		return keys.contains(id);
	}

	@Override
	public Utente findOne(String id) {
		logger.info("Recupero utente");
				
	    DB db = dataSource.getDb();
	    Map<String, Utente> map = db.getTreeMap("utenti2");
	
		Utente utente = map.get(id);
		
		//db.close();		
	
		
		return utente;
	}

	@Override
	public String save(Utente entity) {
		
		logger.info("Salvataggio in corso");
		
		DB db = dataSource.getDb();	
		
		Map<String, Utente> map = db.getTreeMap(mapUtenti);
				
		// if username doesn't exist we'll store entity and return username else return null.
		if (map.get(entity.getUsername())==null) {
			map.put(entity.getUsername(), entity);
			db.commit();
			//db.close();		
			return entity.getUsername();
		} else {
			return null;
		}

	}

	@Override
	public Collection<Utente> getAll() {
		logger.info("Prelevo di tutti i componenti");
		
		DB db = dataSource.getDb();	
		
		Map<String, Utente> map = db.getTreeMap("utenti2");
		
		Collection<Utente> utenti= map.values();
		
		//db.close();
		
		
		return utenti;
	}








}
