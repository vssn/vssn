package it.unibo.cs.sweng.vssn.client.widget;

import java.util.ArrayList;

import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.client.GreetingServiceAsync;
import it.unibo.cs.sweng.vssn.domain.Messaggio;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.TextBox;

public class AdminMessageForm extends Composite {
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	public AdminMessageForm(){
		
		CaptionPanel cptnpnlNewPanel = new CaptionPanel("Gestione Categorie");
		initWidget(cptnpnlNewPanel);
		
		FlexTable flexTable = new FlexTable();
		cptnpnlNewPanel.setContentWidget(flexTable);
		flexTable.setSize("100%", "100%");
		
		
		greetingService.getAllMessaggio(new AsyncCallback<ArrayList<Messaggio>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<Messaggio> result) {
				for(int i=0;i<result.size();i++){
					//listBox.addItem(result.get(i).toString());
				}
				
			}
			
		});
		
		final ListBox listBox = new ListBox();
		flexTable.setWidget(0, 0, listBox);
		listBox.setSize("100%", "100%");
		listBox.setVisibleItemCount(5);
		flexTable.getCellFormatter().setVerticalAlignment(0, 1, HasVerticalAlignment.ALIGN_TOP);
		
		Button btnRemove = new Button("Remove");
		flexTable.setWidget(0, 1, btnRemove);
		
		btnRemove.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				
				
			}
			
		});
		
	}
}
