package it.unibo.cs.sweng.vssn.client.widget;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.widget.client.TextButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.CaptionPanel;

public class AdminMenu extends Composite {
	
	PageLayout mainPage;
	
	public AdminMenu() {
		
		CaptionPanel cptnpnlAdmin = new CaptionPanel("Admin Menu");
		cptnpnlAdmin.setStyleName("adminMenu");
		initWidget(cptnpnlAdmin);
		cptnpnlAdmin.setWidth("100%");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		cptnpnlAdmin.setContentWidget(verticalPanel);
		verticalPanel.setSize("196px", "3cm");
		
		FlexTable flexTable = new FlexTable();
		verticalPanel.add(flexTable);
		flexTable.setWidth("190px");
		
		TextButton txtbtnNewButton = new TextButton("Gestione Categorie");
		flexTable.setWidget(0, 0, txtbtnNewButton);
		
		txtbtnNewButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				mainPage.getCenter().clear();
				mainPage.getCenter().add(new CategorieForm(),DockPanel.CENTER);
				
			}
			
		});
		
		TextButton txtbtnNewButton_1 = new TextButton("Gestione Messaggi");
		
		txtbtnNewButton_1.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				mainPage.getCenter().clear();
				mainPage.getCenter().add(new AdminMessageForm(),DockPanel.CENTER);
				
			}
			
		});
		
		flexTable.setWidget(1, 0, txtbtnNewButton_1);
	}

	public void setMainPage(PageLayout mainPage) {
		this.mainPage = mainPage;
	}

}
