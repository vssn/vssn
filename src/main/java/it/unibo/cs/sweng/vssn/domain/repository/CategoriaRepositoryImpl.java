package it.unibo.cs.sweng.vssn.domain.repository;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CategoriaRepositoryImpl implements CategoriaRepository {
	Logger logger = Logger.getLogger(CategoriaRepositoryImpl.class);
	
	@Autowired
	private DataSource<DB> dataSource;
	
	final private String mapCategorie = "categorie2";
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String delete(String id) {
		logger.info("Deleting Categoria with username: " + id);

	    DB db = dataSource.getDb();
	    Map<String, String> map = db.getTreeMap(mapCategorie);
		
		String categoria = map.remove(id);		
		db.commit();		
		//db.close();
		
		return categoria;
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean exists(String id) {
		logger.info("Checking if " + id + "exists.");
		
	    DB db = dataSource.getDb();
	    Map<String, String> map = db.getTreeMap(mapCategorie);
	
	    Set<String> keys = map.keySet();
	    
	    //db.close();
	    
		return keys.contains(id);
	}

	@Override
	public String findOne(String id) {
		logger.info("Recupero categoria");
		
	    DB db = dataSource.getDb();
	    Map<String, String> map = db.getTreeMap(mapCategorie);
	
		String categoria = map.get(id);
		
		//db.close();		
	
		
		return categoria;
	}

	@Override
	public Collection<String> getAll() {
		logger.info("Prelevo di tutte le categorie");
		
		DB db = dataSource.getDb();	
		
		Map<String, String> map = db.getTreeMap(mapCategorie);
		
		Collection<String> categorie= map.values();
		
		//db.close();
		
		
		return categorie;
	}

	@Override
	public String save(String entity) {
		logger.info("Salvataggio in corso");
		
		DB db = dataSource.getDb();	
		
		Map<String, String> map = db.getTreeMap(mapCategorie);
				
		// if category doesn't exist we'll store entity and return category else return null.
		if (map.get(entity)==null) {
			map.put(entity, entity);
			db.commit();
			//db.close();		
			return entity;
		} else {
			return null;
		}
	}

}
