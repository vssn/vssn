package it.unibo.cs.sweng.vssn.shared;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;

public class MyFilter implements Filter {
	
	Logger logger = Logger.getLogger(MyFilter.class);
	
	@Override
	public void destroy() {
		logger.info("destroy filter done");
		
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		
			logger.info("do filter done");
		
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		logger.info("init filter done");
		
	}

}
