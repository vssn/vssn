package it.unibo.cs.sweng.vssn.domain.repository;

import it.unibo.cs.sweng.vssn.domain.Utente;

public interface UtenteRepository extends CrudRepository<String, Utente> {

}
