package it.unibo.cs.sweng.vssn.client;


import it.unibo.cs.sweng.vssn.client.widget.PageLayout;
import it.unibo.cs.sweng.vssn.client.widget.SideBar;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Vssn implements EntryPoint {


	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);

	/**
	 * This is the entry point method.
	 */
	
	
	
	public void onModuleLoad() {

		

		
		SideBar sideBar = new SideBar();
		
		final PageLayout pageLayout = new PageLayout();
		

		pageLayout.getEast().add(sideBar);


		
		// Use RootPanel.get() to get the entire body element
		RootPanel.get().add(pageLayout);
		

		

		
	}
}
