package it.unibo.cs.sweng.vssn.client.widget;


import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.client.GreetingServiceAsync;
import it.unibo.cs.sweng.vssn.domain.Utente;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Hidden;


public class PageLayout extends Composite {
	
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	private DockPanel center;				//CENTER
	private VerticalPanel east;		//EAST
	private MainMenu mainMenu;
	
	private MessageBoard messageBoard;
	
	public PageLayout() {
		
		
		/*
		 * *************************************
		 * 			MAIN DOCK and MENUBAR
		 * ************************************* 
		 */
		
		DockPanel dockPanel = new DockPanel();
		initWidget(dockPanel);
		dockPanel.setSize("100%", "100%");
		
		dockPanel.setStyleName("mainPage");
		
		SimplePanel simplePanel = new SimplePanel();
		simplePanel.setStyleName("northPanel");
		dockPanel.add(simplePanel, DockPanel.NORTH);
		simplePanel.setSize("100%", "41px");
		dockPanel.setCellHeight(simplePanel, "50px");
		
		Label lblVerySimpleSocial = new Label("Very Simple Social Network");
		lblVerySimpleSocial.setStyleName("gwt-Label-header");
		simplePanel.setWidget(lblVerySimpleSocial);
		lblVerySimpleSocial.setSize("100%", "100%");
		
		/*
		 * **********************************************
		 * 					WEST PANEL
		 * **********************************************
		 */
		
		final VerticalPanel west = new VerticalPanel();
		
	
		dockPanel.add(west, DockPanel.WEST);
		dockPanel.setCellWidth(west, "25%");
		west.setWidth("100%");
		
		SearchMenu searchMenu = new SearchMenu();
		searchMenu.setMainPage(this);
		west.add(searchMenu);
		
		final AdminMenu adminMenu = new AdminMenu();
		adminMenu.setMainPage(this);
		adminMenu.setWidth("224px");
		
		greetingService.loginFromSessionServer(new AsyncCallback<Utente>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(Utente result) {
				
				if(result!=null && result.getUsername().equals("admin")){
					west.add(adminMenu);
					
				}
			}
			
		});
		

		

		
		
		

		
		
		
		/*
		 * **************************************
		 * 				EAST PANEL
		 * **************************************
		 */
		
		final UtentiMenu utentiMenu = new UtentiMenu();
		utentiMenu.setMainPage(this);
		
		east = new VerticalPanel();
		dockPanel.add(east, DockPanel.EAST);
		dockPanel.setCellWidth(east, "25%");
		east.setWidth("201px");
		
		mainMenu = new MainMenu();
		mainMenu.setStyleName("mainMenu");
		mainMenu.setMainPage(this);
		east.add(mainMenu);
		mainMenu.setWidth("196px");
		
		// *** comboBoxFollowing ***
		
		greetingService.loginFromSessionServer(new AsyncCallback<Utente>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("ERROR OCCURRED");
				
			}

			@Override
			public void onSuccess(Utente result) {
				
				if (result != null) {
					

					east.add(utentiMenu);
					
					
				}
				
			}
			
		});
		
		
		/*
		 * **************************************
		 * 				CENTER PANEL
		 * **************************************
		 */
		
		messageBoard = new MessageBoard();
		
		center = new DockPanel();
		center.setStyleName("postMessage");
		center.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		dockPanel.add(center, DockPanel.CENTER);
		dockPanel.setCellHorizontalAlignment(center, HasHorizontalAlignment.ALIGN_CENTER);
		dockPanel.setCellWidth(center, "75%");
		center.setWidth("70%");
		

		
		
		
		/*
		 * **********************************************
		 * 					SOUTH PANEL
		 * **********************************************
		 */
		HorizontalPanel south = new HorizontalPanel();
		south.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		dockPanel.add(south, DockPanel.SOUTH);
		

		

	}
	
	public VerticalPanel getEast(){
		return east;
	}
	
	public DockPanel getCenter(){
		return center;
	}

	public MessageBoard getMessageBoard() {
		return messageBoard;
	}

	public void setMessageBoard(MessageBoard messageBoard) {
		this.messageBoard = messageBoard;
	}
	

}
