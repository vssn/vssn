package it.unibo.cs.sweng.vssn.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.domain.Messaggio;
import it.unibo.cs.sweng.vssn.domain.Utente;
import it.unibo.cs.sweng.vssn.domain.repository.CategoriaRepository;
import it.unibo.cs.sweng.vssn.domain.repository.FollowingRepository;
import it.unibo.cs.sweng.vssn.domain.repository.MessaggioRepository;
import it.unibo.cs.sweng.vssn.domain.repository.UtenteRepository;
import it.unibo.cs.sweng.vssn.shared.FieldVerifier;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.gwt.ss.client.exceptions.GwtSecurityException;



/**
 * The server-side implementation of the RPC service.
 */
@Service("greet")
public class GreetingServiceImpl extends RemoteServiceServlet implements	GreetingService {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5229784821947836843L;

	Logger logger = Logger.getLogger(GreetingServiceImpl.class);
	
	@Autowired
	private UtenteRepository utenteRepository;
	
	@Autowired
	private MessaggioRepository messaggioRepository;
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private FollowingRepository followingRepository;
	
	@Autowired
	private HttpServletRequest hsr;
	
	public String greetServer(String input) throws IllegalArgumentException {
		logger.info("Starting serving request");
		// Verify that the input is valid. 
		if (!FieldVerifier.isValidName(input)) {
			// If the input is not valid, throw an IllegalArgumentException back to
			// the client.
			throw new IllegalArgumentException(
					"Name must be at least 4 characters long");
		}


		return "Hello";
	}

	@Override
	public void passaUtente(Utente utente) {
		logger.info("Passato utente: " + utente.toString());
		
		//save utente
		utenteRepository.save(utente);
	}

	@Override
	public void salvaMessaggio(Messaggio messaggio) {
		Utente utente = getUserAlreadyFromSession();
		messaggio.setIdUtente(utente.getUsername());
		messaggioRepository.save(messaggio);
		
	}

	@Override
	public ArrayList<Messaggio> getAllMessaggio() {
		ArrayList<Messaggio> result = new ArrayList<Messaggio>(messaggioRepository.getAll());
		
		return result;
	}

	@Override
	public Utente login(String username, String password) {
		
		Utente utente = utenteRepository.findOne(username);
		
				
		if (utente != null && utente.getPassword().equals(password)){
			
			// registro l'utente nella sessione
			// e restituisco sessionID
			storeUserInSession(utente);
			return utente;
		} else {
			return null;
		}
	}
	
    @Override
    public Utente loginFromSessionServer()
    {
        return getUserAlreadyFromSession();
    }
	
    private Utente getUserAlreadyFromSession()
    {
    	
    	Utente user = null;
    	
    	
    	//ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
    	  if (hsr != null) {
    	       //HttpServletRequest httpServletRequest = servletContext;
    	       
    	        //HttpServletRequest httpServletRequest = this.getThreadLocalRequest();
    	        
    	       
    	        
    	        HttpSession session = hsr.getSession();
    	        Object userObj = session.getAttribute("user");
    	        if (userObj != null) // && userObj instanceof Utente)
    	        {
    	            user = (Utente) userObj;
    	        }
    	       
    	       //remote_user = req.getSession().getAttribute("j_username").toString();
    	  }
    	
    	
        

        return user;
    }

	private void storeUserInSession(Utente utente){
		logger.info("Storing user in session");
        //HttpServletRequest httpServletRequest = this.getThreadLocalRequest();
        HttpSession session = hsr.getSession(true);
        session.setAttribute("user", utente);
        
        logger.info(session.getAttribute("user").toString());
	}

	@Override
	public ArrayList<Utente> getAllUtente() {
		ArrayList<Utente> utenti = new ArrayList<Utente>(utenteRepository.getAll());
		return utenti;
	}

	@Override
	public ArrayList<String> getAllCategoria() throws GwtSecurityException {
		ArrayList<String> categorie = new ArrayList<String>(categoriaRepository.getAll());
		return categorie;
	}

	@Override
	public void salvaCategoria(String categoria) {
		categoriaRepository.save(categoria);
		
	}

	@Override
	public void deleteCategoria(String categoria) {
		categoriaRepository.delete(categoria);
		
	}

	@Override
	public void logoutFromSessionServer() {
    	
    	
    	//ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
    	  if (hsr != null) {

    	        HttpSession session = hsr.getSession();
    	        session.removeAttribute("user");
    	  }
		
	}

	@Override
	public void addFollowing(String following) {
		Utente follower = getUserAlreadyFromSession();
		followingRepository.addFollowing(follower.getUsername(), following);
		
	}

	@Override
	public ArrayList<String> getAllFollowing() {
		Utente follower = getUserAlreadyFromSession();		
		return followingRepository.getAllByUsername(follower.getUsername());
	}

	@Override
	public void removeFollowing(String following) {
		Utente follower = getUserAlreadyFromSession();
		followingRepository.removeFollowing(follower.getUsername(), following);
		
	}

	@Override
	public void deleteMessaggio(int idMessaggio) {
		messaggioRepository.delete(idMessaggio);
		
	}
}
