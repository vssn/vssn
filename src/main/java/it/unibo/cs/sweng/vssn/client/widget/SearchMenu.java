package it.unibo.cs.sweng.vssn.client.widget;

import java.util.ArrayList;

import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.client.GreetingServiceAsync;
import it.unibo.cs.sweng.vssn.domain.Utente;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.widget.client.TextButton;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ListBox;

public class SearchMenu extends Composite {
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	private ListBox comboBox;
	private ListBox comboBoxFollowing;
	private Label lblFiltraPerCategorai;
	
	private PageLayout mainPage;
	
	public SearchMenu() {
		
		CaptionPanel cptnpnlFiltri = new CaptionPanel();
		cptnpnlFiltri.setStyleName("searchMenu");
		cptnpnlFiltri.setCaptionHTML("Filtri");
		cptnpnlFiltri.setCaptionText("Filtri");
		initWidget(cptnpnlFiltri);
		
		final VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setStyleName((String) null);
		verticalPanel.setWidth("100%");
		
		cptnpnlFiltri.add(verticalPanel);
		
		TextButton txtbtnFiltra = new TextButton("Filtra");
		verticalPanel.add(txtbtnFiltra);
		
		txtbtnFiltra.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				greetingService.loginFromSessionServer(new AsyncCallback<Utente>(){

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(Utente result) {
						if(result==null){
							mainPage.getMessageBoard().setBoard(comboBox.getItemText(comboBox.getSelectedIndex()), "ALL");
						} else {
							mainPage.getMessageBoard().setBoard(comboBox.getItemText(comboBox.getSelectedIndex()), comboBoxFollowing.getItemText(comboBoxFollowing.getSelectedIndex()));
						}
						
					}
					
				});
				
				
				
			}
			
		});
		
		FlexTable flexTable = new FlexTable();
		verticalPanel.add(flexTable);
		
		lblFiltraPerCategorai = new Label("Filtra per Categoria");
		flexTable.setWidget(0, 0, lblFiltraPerCategorai);
		
		comboBox = new ListBox();
		flexTable.setWidget(1, 0, comboBox);
		
		comboBox.addItem("ALL");
		
		greetingService.getAllCategoria(new AsyncCallback<ArrayList<String>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				for(int i=0;i<result.size();i++){
					comboBox.addItem(result.get(i));
				}
				
			}
			
		});
		
		
		// *** Filtro per following
		
		greetingService.loginFromSessionServer(new AsyncCallback<Utente>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(Utente result) {
				
				if (result!=null){
					FlexTable flexTableFollowing = new FlexTable();
					verticalPanel.add(flexTableFollowing);
					
					Label lblFiltraPerFollowing = new Label("Filtra per Following");
					flexTableFollowing.setWidget(0, 0, lblFiltraPerFollowing);
					
					comboBoxFollowing = new ListBox();
					flexTableFollowing.setWidget(1, 0, comboBoxFollowing);
					
					comboBoxFollowing.addItem("ALL");
					
					
					
					greetingService.getAllFollowing(new AsyncCallback<ArrayList<String>>(){

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
							
						}

						@Override
						public void onSuccess(ArrayList<String> result) {
							for(int i=0;i<result.size();i++){
								comboBoxFollowing.addItem(result.get(i));
							}
							
						}
						
					});
					
					
				}

				
			}
			
		});
		

	}

	public void setMainPage(PageLayout mainPage) {
		this.mainPage = mainPage;
	}
	




}
