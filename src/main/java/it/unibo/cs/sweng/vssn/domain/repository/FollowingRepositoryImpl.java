package it.unibo.cs.sweng.vssn.domain.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FollowingRepositoryImpl implements FollowingRepository {
	Logger logger = Logger.getLogger(CategoriaRepositoryImpl.class);
	
	@Autowired
	private DataSource<DB> dataSource;
	
	final private String mapFollowing = "following";
	
	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ArrayList<String> delete(String id) {
		logger.info("Deleting Categoria with username: " + id);

	    DB db = dataSource.getDb();
	    Map<String, ArrayList<String>> map = db.getTreeMap(mapFollowing);
		
		ArrayList<String> following = map.remove(id);	
		db.commit();		
		
		return following;
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub

	}
	
	// TODO DA RIVEDERE IMPLEMENTAZIONE
	@Override
	public boolean exists(String id) {
		logger.info("Checking if " + id + "exists.");
		
	    DB db = dataSource.getDb();
	    Map<String, ArrayList<String>> map = db.getTreeMap(mapFollowing);
	
	    Set<String> keys = map.keySet();
	    
		return keys.contains(id);
	}

	@Override
	public ArrayList<String> findOne(String id) {
		logger.info("Recupero following");
		
	    DB db = dataSource.getDb();
	    Map<String, ArrayList<String>> map = db.getTreeMap(mapFollowing);
	
		ArrayList<String> following = map.get(id);
		
		return following;
	}

	@Override
	public Collection<ArrayList<String>> getAll() {
		logger.info("Prelevo di tutte le liste di following");
		
		DB db = dataSource.getDb();	
		
		Map<String, ArrayList<String>> map = db.getTreeMap(mapFollowing);
		
		Collection<ArrayList<String>> followings= map.values();

		return followings;
	}

	@Override
	public String save(ArrayList<String> entity) {
		return null;
	}

	@Override
	public void addFollowing(String follower, String following) {

		DB db = dataSource.getDb();	
		
		Map<String, ArrayList<String>> map = db.getTreeMap(mapFollowing);
		
		if(exists(follower)){
			ArrayList<String> followingList = map.get(follower);
			
			if (!followingList.contains(following)){
				followingList.add(following);
				db.commit();
			}
			
		} else {
			ArrayList<String> followingListNew = new ArrayList<String>();
			
			followingListNew.add(following);
			
			map.put(follower, followingListNew);
			db.commit();
			
		}
		
	}

	@Override
	public ArrayList<String> getAllByUsername(String username) {
		DB db = dataSource.getDb();	
		
		Map<String, ArrayList<String>> map = db.getTreeMap(mapFollowing);
		
		return map.get(username);
	}

	@Override
	public void removeFollowing(String follower, String following) {
		DB db = dataSource.getDb();	
		
		Map<String, ArrayList<String>> map = db.getTreeMap(mapFollowing);
		
		if(exists(follower)){
			ArrayList<String> followingList = map.get(follower);
			
			if (followingList.contains(following)){
				followingList.remove(following);
				db.commit();
			}
			
		} 
		
	}
	
	

}
