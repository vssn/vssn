package it.unibo.cs.sweng.vssn.domain.repository;


import static org.junit.Assert.*;
import it.unibo.cs.sweng.vssn.domain.Utente;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext.xml"})
@WebAppConfiguration (value = "war")
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class UtenteRepositoryImplTest {

	@Autowired
	UtenteRepository utenteRepository;
	
	private static Utente utenteTest;
	private static String username;
	private static int numUtenti;
	

	@BeforeClass
	public static void setUp(){
		System.out.println("inizializzo");
		username = "utenteTest";
		utenteTest = new Utente();		
		utenteTest.setUsername(username);
		
		//utenteRepository.delete(utenteTest.getUsername());		
		//numUtenti = utenteRepository.getAll().size();
	}
	
	
	@Test 
	public void atestSave(){		
		//assertTrue(utenteRepository.getAll().size()==numUtenti);
		assertTrue(utenteRepository.save(utenteTest).equals(username));
		//assertTrue(utenteRepository.getAll().size()==numUtenti+1);
		
	}
	
	@Test
	public void btestExists(){
		assertTrue(utenteRepository.exists(username));
	}
	
	@Test
	public void ctestFindOne(){
		assertNotNull(utenteRepository.findOne(username).getUsername().equals(username));
	}
	
	@Test
	public void dtestGetAll() {
		assertTrue(utenteRepository.getAll().size()>0);
	}
	
	@Test 
	public void etestDelete(){
		//assertTrue(utenteRepository.getAll().size()==numUtenti+1);
		assertTrue(utenteRepository.delete(username).getUsername().equals(username));
		//assertTrue(utenteRepository.getAll().size()==numUtenti);
	}
	


}
