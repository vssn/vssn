package it.unibo.cs.sweng.vssn.domain.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext.xml" })
@WebAppConfiguration(value = "war")
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class CategoriaRepositoryImplTest {

	@Autowired
	CategoriaRepository categoriaRepository;

	private static String categoriaTest;
	private static int numCategorie;

	@BeforeClass
	public static void setUp() {
		
		categoriaTest = "categoriaTest";
	}

	@Test
	public void atestSave() {
		
		assertTrue(categoriaRepository.save(categoriaTest).equals(categoriaTest));
		

	}

	@Test
	public void btestExists() {
		assertTrue(categoriaRepository.exists(categoriaTest));
	}

	@Test
	public void ctestFindOne() {
		assertNotNull(categoriaRepository.findOne(categoriaTest).equals(categoriaTest));
	}

	@Test
	public void dtestGetAll() {
		assertTrue(categoriaRepository.getAll().size()>0);
	}

	@Test
	public void etestDelete() {
		assertTrue(categoriaRepository.delete(categoriaTest).equals(categoriaTest));
		
	}

}
