package it.unibo.cs.sweng.vssn.server;

import static org.junit.Assert.*;
import it.unibo.cs.sweng.vssn.client.GreetingService;
import it.unibo.cs.sweng.vssn.domain.Messaggio;
import it.unibo.cs.sweng.vssn.domain.Utente;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext.xml"})
@WebAppConfiguration (value = "war")
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class GreetingServiceImplTest {

	@Autowired
	private GreetingService s;
	
	@Autowired
	private HttpServletRequest hsr;
	
	@Test
	public void testGetAllMessaggio() {
		
		
		ArrayList<Messaggio> messaggi = s.getAllMessaggio();
		
		assertNotNull(messaggi);
		assertTrue(messaggi.size()>0);
	}
	
	@Test
	public void TestLogin(){
		Utente u = s.login("D", "D");
		assertTrue(u.getUsername().equals("D"));
	}
	
	@Test
	public void TestStoreUserInSession(){
		s.login("D", "D");
		HttpSession session = hsr.getSession();
		assertNotNull(session.getAttribute("user"));
	}
	
	@Test
	public void TestLoginFromSessionServer(){
		s.login("D", "D");
		
		assertNotNull(s.loginFromSessionServer());
		assertTrue(s.loginFromSessionServer() instanceof Utente);
	}

}
