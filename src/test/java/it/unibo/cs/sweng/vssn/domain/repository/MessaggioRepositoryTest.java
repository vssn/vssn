package it.unibo.cs.sweng.vssn.domain.repository;

import it.unibo.cs.sweng.vssn.domain.Messaggio;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext.xml"})
@WebAppConfiguration(value="war")		// war refers to war folder
public class MessaggioRepositoryTest {

	@Autowired
	MessaggioRepository messaggioRepository;
	
	
	@Test
	public void saveTest(){
		Messaggio messaggio = new Messaggio();
		
		messaggio.setCategoria("categoriaTest");		
		messaggio.setIdUtente("Utente test");
		messaggio.setMsg("Ciao ciao");
		
		messaggioRepository.save(messaggio);
		
	}
	
}
